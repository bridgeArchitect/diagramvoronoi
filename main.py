import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from random import randint

# open file
file_reader = open("results.txt")

# initialization
polygons = []
x = list()
y = list()

# read the first line of file
i = 0
n = 0
line = file_reader.readline()
while line:

    if i % 2 == 0:
        # read center of polygon
        substrings = line.split(" ")
        x.append(float(substrings[0]))
        y.append(float(substrings[1]))
    else:
        
        # read points of polygon
        substrings = line.split(" ")
        i1 = 0
        polygon = []
        while i1 + 1 < len(substrings):
            polygon.append([float(substrings[i1]), float(substrings[i1 + 1])])
            i1 = i1 + 2
        
        # transform list of points to "np" array
        points = np.array(polygon)

        # create polygon of random color using "np" array
        rand_number = randint(1, 4)
        p = None
        if rand_number == 1:
            p = Polygon(points, facecolor = 'g')
        elif rand_number == 2:
            p = Polygon(points, facecolor = 'b')
        elif rand_number == 3:
            p = Polygon(points, facecolor = 'r')
        elif rand_number == 4:
            p = Polygon(points, facecolor = 'y')

        # add new polygon to list
        polygons.append(p)
        n = n + 1

    # read new line of file
    line = file_reader.readline()
    i = i + 1

# create plot
fig, ax = plt.subplots()

# add polygon on plot
i = 0
while i < n:
    ax.add_patch(polygons[i])
    i = i + 1

# set limits of plot
ax.set_xlim([0,1])
ax.set_ylim([0,1])

# add centers of polygons on plot
plt.plot(x, y, 's')

# show plot
plt.show()
