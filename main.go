package main

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"os"
	"strconv"
)

const (
	/* constant to set methods of specifying points */
	constMethod  = "const"
	randomMethod = "random"
	/* small number for comparison */
	smallNumber = 0.00001
	/* name of file to write results */
	resFile = "results.txt"
)

/* structure to describe point */
type Point struct {
	x, y float64
}

/* structure to describe straight line */
type StraightLine struct {
	k, b float64
}

/* structure to describe polygon */
type Polygon struct {
	points []Point
	center Point
}

/* gaps for Shell sort */
var (
	interval = [7]int{301, 132, 57, 23, 10, 4, 1}
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to find normal line to the given cut-off */
func findNormal(point1, point2 Point) StraightLine {

	var (
		lineAnswer StraightLine
	)

	/* find coefficients of normal line */
	lineAnswer.k = -(point2.x - point1.x) / (point2.y - point1.y)
	lineAnswer.b = (point1.y + point2.y - lineAnswer.k*(point1.x+point2.x)) / 2
	/* return answer */
	return lineAnswer

}

/* function to find intersection of two lines */
func findIntersectionTwoLines(line1, line2 StraightLine) Point {

	var (
		pointAnswer Point
	)

	/* find coordinates of point */
	pointAnswer.x = (line2.b - line1.b) / (line1.k - line2.k)
	pointAnswer.y = line1.k*pointAnswer.x + line1.b
	/* return answer */
	return pointAnswer

}

/* function to find intersection of line with y = 0 */
func findIntersectionYZero(line StraightLine) Point {

	var (
		pointAnswer Point
	)

	/* find coordinates of point */
	pointAnswer.x = -line.b / line.k
	pointAnswer.y = 0.0
	/* return answer */
	return pointAnswer

}

/* function to find intersection of line with x = 1 */
func findIntersectionXOne(line StraightLine) Point {

	var (
		pointAnswer Point
	)

	/* find coordinates of point */
	pointAnswer.x = 1.0
	pointAnswer.y = line.k + line.b
	/* return answer */
	return pointAnswer

}

/* function to find intersection of line with y = 1 */
func findIntersectionYOne(line StraightLine) Point {

	var (
		pointAnswer Point
	)

	/* find coordinates of point */
	pointAnswer.x = (1.0 - line.b) / line.k
	pointAnswer.y = 1.0
	/* return answer */
	return pointAnswer

}

/* function to find intersection of line with x = 0 */
func findIntersectionXZero(line StraightLine) Point {

	var (
		pointAnswer Point
	)

	/* find coordinates of point */
	pointAnswer.x = 0.0
	pointAnswer.y = line.b
	/* return answer */
	return pointAnswer

}

/* function to check whether two points (with boundary condition) */
func checkHalfPlaneCondition(point1, point2 Point, line StraightLine) bool {

	/* check boundary condition */
	if math.Abs(point1.y-line.k*point1.x-line.b) < smallNumber ||
		math.Abs(point2.y-line.k*point2.x-line.b) < smallNumber {
		return true
	}

	/* return bool value of checking */
	return (point1.y-line.k*point1.x-line.b)*(point2.y-line.k*point2.x-line.b) >= 0.0

}

/* function to calculate angle of point in polar system */
func calculateAnglePolarSystem(point0, point Point) float64 {

	var (
		phi float64
	)

	/* calculate angle using ArcCos function */
	if point.y-point0.y >= 0 {
		phi = math.Acos((point.x - point0.x) /
			math.Sqrt((point.x-point0.x)*(point.x-point0.x)+(point.y-point0.y)*(point.y-point0.y)))
	} else {
		phi = 2.0*math.Pi - math.Acos((point.x-point0.x)/
			math.Sqrt((point.x-point0.x)*(point.x-point0.x)+(point.y-point0.y)*(point.y-point0.y)))
	}

	/* return answer */
	return phi

}

/* function to sort points using angles */
func sortPointsUsingAngle(points *[]Point, angles *[]float64) {

	/* declarations of variables */
	var (
		i, i1, i2 int
		currAngle float64
		currX     float64
		currY     float64
	)

	/* make Shell sort */
	i = len(interval) - 1
	for i >= 0 {
		i1 = interval[i]
		for i1 < len(*angles) {
			i2 = i1 - interval[i]
			for i2 >= 0 {
				/* swap two elements if condition is true */
				if (*angles)[i2] > (*angles)[i2+interval[i]] {
					/* swap angle */
					currAngle = (*angles)[i2]
					(*angles)[i2] = (*angles)[i2+interval[i]]
					(*angles)[i2+interval[i]] = currAngle
					/* swap coordinates */
					currX = (*points)[i2].x
					(*points)[i2].x = (*points)[i2+interval[i]].x
					(*points)[i2+interval[i]].x = currX
					currY = (*points)[i2].y
					(*points)[i2].y = (*points)[i2+interval[i]].y
					(*points)[i2+interval[i]].y = currY
				}
				i2 -= interval[i]
			}
			i1++
		}
		i--
	}

}

/* function to find points of polygon for the given center */
func findPoints(points []Point, idPoint int) []Point {

	var (
		i, i1, i2                   int
		lines                       []StraightLine
		currPoint                   Point
		currPoint1, currPoint2      Point
		currPoint3, currPoint4      Point
		answerPoints                []Point
		ind, ind1, ind2, ind3, ind4 bool
		phi                         []float64
	)

	/* create array of lines and answer points */
	lines = make([]StraightLine, 0, len(points)-1)
	answerPoints = make([]Point, 0, len(points))

	/* find all lines of specific point */
	i = 0
	for i < len(points) {
		if i != idPoint {
			lines = append(lines, findNormal(points[i], points[idPoint]))
		}
		i++
	}

	/* go through all couples of lines */
	i = 0
	for i < len(lines) {
		i1 = i + 1
		for i1 < len(lines) {

			/* find intersection of lines */
			currPoint = findIntersectionTwoLines(lines[i], lines[i1])

			/* check point using half-plane condition */
			i2 = 0
			ind = true
			for i2 < len(lines) {
				if !checkHalfPlaneCondition(currPoint, points[idPoint], lines[i2]) {
					ind = false
					break
				}
				i2++
			}

			/* add point to answer if half-plane condition is true */
			if ind {
				answerPoints = append(answerPoints, currPoint)
			}
			i1++

		}
		i++
	}

	/* go through all lines */
	i = 0
	for i < len(lines) {

		/* find intersections with edges of square */
		currPoint1 = findIntersectionXOne(lines[i])
		currPoint2 = findIntersectionXZero(lines[i])
		currPoint3 = findIntersectionYOne(lines[i])
		currPoint4 = findIntersectionYZero(lines[i])

		/* check half-plane condition for point of intersection with x = 1 */
		i2 = 0
		ind1 = true
		for i2 < len(lines) {
			if !checkHalfPlaneCondition(currPoint1, points[idPoint], lines[i2]) {
				ind1 = false
				break
			}
			i2++
		}
		/* check boundary condition for point of intersection with x = 1 */
		if (currPoint1.x > 1.0) || (currPoint1.x < 0.0) {
			ind1 = false
		}
		if (currPoint1.y > 1.0) || (currPoint1.y < 0.0) {
			ind1 = false
		}

		/* check half-plane condition for point of intersection with x = 0 */
		i2 = 0
		ind2 = true
		for i2 < len(lines) {
			if !checkHalfPlaneCondition(currPoint2, points[idPoint], lines[i2]) {
				ind2 = false
				break
			}
			i2++
		}
		/* check boundary condition for point of intersection with x = 0 */
		if (currPoint2.x > 1.0) || (currPoint2.x < 0.0) {
			ind2 = false
		}
		if (currPoint2.y > 1.0) || (currPoint2.y < 0.0) {
			ind2 = false
		}

		/* check half-plane condition for point of intersection with y = 1 */
		i2 = 0
		ind3 = true
		for i2 < len(lines) {
			if !checkHalfPlaneCondition(currPoint3, points[idPoint], lines[i2]) {
				ind3 = false
				break
			}
			i2++
		}
		/* check boundary condition for point of intersection with y = 1 */
		if (currPoint3.x > 1.0) || (currPoint3.x < 0.0) {
			ind3 = false
		}
		if (currPoint3.y > 1.0) || (currPoint3.y < 0.0) {
			ind3 = false
		}

		/* check half-plane condition for point of intersection with y = 0 */
		i2 = 0
		ind4 = true
		for i2 < len(lines) {
			if !checkHalfPlaneCondition(currPoint4, points[idPoint], lines[i2]) {
				ind4 = false
				break
			}
			i2++
		}
		/* check boundary condition for point of intersection with y = 0 */
		if (currPoint4.x > 1.0) || (currPoint4.x < 0.0) {
			ind4 = false
		}
		if (currPoint4.y > 1.0) || (currPoint4.y < 0.0) {
			ind4 = false
		}

		/* add point of intersection with x = 1 to answer if conditions are true */
		if ind1 {
			answerPoints = append(answerPoints, currPoint1)
		}
		/* add point of intersection with x = 0 to answer if conditions are true */
		if ind2 {
			answerPoints = append(answerPoints, currPoint2)
		}
		/* add point of intersection with y = 1 to answer if conditions are true */
		if ind3 {
			answerPoints = append(answerPoints, currPoint3)
		}
		/* add point of intersection with y = 0 to answer if conditions are true */
		if ind4 {
			answerPoints = append(answerPoints, currPoint4)
		}

		i++

	}

	/* go through all points */
	i = 0
	ind1 = false
	ind2 = false
	ind3 = false
	ind4 = false
	for i < len(answerPoints) {
		/* check whether point is located on edge of square */
		if math.Abs(answerPoints[i].x) < smallNumber {
			ind1 = true
		} else if math.Abs(answerPoints[i].y) < smallNumber {
			ind2 = true
		} else if math.Abs(answerPoints[i].x - 1.0) < smallNumber {
			ind3 = true
		} else if math.Abs(answerPoints[i].y - 1.0) < smallNumber {
			ind4 = true
		}
		i++
	}

	/* add new edge point if it is necessary */
	if ind1 && ind2 {
		answerPoints = append(answerPoints, Point{0.0, 0.0})
	} else if ind3 && ind2 {
		answerPoints = append(answerPoints, Point{1.0, 0.0})
	} else if ind3 && ind4 {
		answerPoints = append(answerPoints, Point{1.0, 1.0})
	} else if ind1 && ind4 {
		answerPoints = append(answerPoints, Point{0.0, 1.0})
	}

	/* calculate angles of points relative to center of polygon */
	phi = make([]float64, len(answerPoints))
	i = 0
	for i < len(phi) {
		phi[i] = calculateAnglePolarSystem(points[idPoint], answerPoints[i])
		i++
	}

	/* sort points using angles relative to center of polygon*/
	sortPointsUsingAngle(&answerPoints, &phi)
	/* return points */
	return answerPoints

}

/* function to find polygons using array of points */
func findPolygons(points []Point) []Polygon {

	var (
		polygons   []Polygon
		currPoints []Point
		i, i1      int
	)

	/* create array of polygons and initialize it */
	polygons = make([]Polygon, len(points))
	i = 0
	for i < len(points) {
		polygons[i].points = make([]Point, 0, len(points))
		i++
	}

	/* go through all points */
	i = 0
	for i < len(points) {
		/* find one polygon for specific points */
		currPoints = findPoints(points, i)
		/* save data about new polygon */
		polygons[i].center = points[i]
		i1 = 0
		for i1 < len(currPoints) {
			polygons[i].points = append(polygons[i].points, currPoints[i1])
			i1++
		}
		i++
	}

	/* return array of polygons */
	return polygons

}

/* function to write message and read string */
func readString(row *string, msg string) {

	var (
		err error
	)

	/* write message and read string */
	fmt.Println(msg)
	_, err = fmt.Scanln(row)
	handleError(err)

}

/* function to write message and read integer number */
func readInt(number *int, msg string) {

	var (
		err error
	)

	/* write message and read integer number */
	fmt.Println(msg)
	_, err = fmt.Scanln(number)
	handleError(err)

}

/* function to write answer */
func writeAnswer(polygons []Polygon) {

	var (
		i, i1      int
		fileOutput *os.File
		err        error
	)

	/* create file to write results */
	fileOutput, err = os.Create(resFile)
	handleError(err)

	i = 0
	for i < len(polygons) {
		/* write coordinates of polygon center */
		_, err = fileOutput.WriteString(strconv.FormatFloat(polygons[i].center.x, 'f', -1, 64) + " " +
			strconv.FormatFloat(polygons[i].center.y, 'f', -1, 64) + "\n")
		handleError(err)
		/* go through all points */
		i1 = 0
		for i1 < len(polygons[i].points) {
			/* write coordinates of points to specify polygon */
			_, err = fileOutput.WriteString(strconv.FormatFloat(polygons[i].points[i1].x, 'f', -1, 64) + " " +
				strconv.FormatFloat(polygons[i].points[i1].y, 'f', -1, 64) + " ")
			i1++
		}
		_, err = fileOutput.WriteString("\n")
		handleError(err)
		i++
	}

}

/* entry point */
func main() {

	/* declarations of variables */
	var (
		N         int
		i         int
		method    string
		points    []Point
		polygons  []Polygon
	)

	/* read method to specify points and number of points */
	readString(&method, "Method to specify points:")
	if method == constMethod {
		N = 5
	} else {
		readInt(&N, "Number of points:")
	}

	/* create array of points */
	points = make([]Point, N)

	/* specify points */
	if method == constMethod {

		/* constant values */
		points[0].x = 0.1
		points[0].y = 0.9

		points[1].x = 0.2
		points[1].y = 0.3

		points[2].x = 0.5
		points[2].y = 0.5

		points[3].x = 0.7
		points[3].y = 0.2

		points[4].x = 0.8
		points[4].y = 0.8

	} else if method == randomMethod {

		/* random values */
		i = 0
		for i < N {
			points[i].x = rand.Float64()
			points[i].y = rand.Float64()
			i++
		}

	}

	/* find polygons */
	polygons = findPolygons(points)
	/* write answer */
	writeAnswer(polygons)

}
